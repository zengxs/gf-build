FROM golang
MAINTAINER ZENGXS <xiangsong.zeng@outlook.com>

ENV MCR_ROOT /usr/lib/mcr711
ENV GF_ROOT /go/src/grainforecast
ENV LD_LIBRARY_PATH ./lib:$MCR_ROOT/runtime/glnxa64

COPY . $GF_ROOT
WORKDIR $GF_ROOT

RUN set -ex && \
    ./gitclone.sh && \
    ./runtime.sh && \
    go test && \
    go build

ENV LC_ALL zh_CN.UTF-8
EXPOSE 9090

CMD ./grainforecast
