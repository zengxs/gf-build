#!/bin/sh -ex
curl -LO https://github.com/git-lfs/git-lfs/releases/download/v2.3.4/git-lfs-linux-amd64-2.3.4.tar.gz
tar -xf git-lfs-linux-amd64-2.3.4.tar.gz -C /tmp --strip-components=1
install /tmp/git-lfs /usr/bin

git clone https://bitbucket.org/zengxs/grainforecast.git $GF_ROOT
